<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', 'ifsc\IfscController@test');
Route::post('/ifsc/verifyCode', 'ifsc\IfscController@verifyCode');
Route::post('/ifsc/fetchAllIfsc', 'ifsc\IfscController@getAllBankCodes');
