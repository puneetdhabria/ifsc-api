<?php
/**
 * Created by PhpStorm.
 * User: puneet
 * Date: 2/2/17
 * Time: 1:09 AM
 */

namespace App\Http\Controllers\ifsc;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Http\Models\IfscModel;


class IfscController extends Controller
{
    public function test()
    {
        echo "You have entered the IFSC API";
    }

    function verifyCode(Request $request)
    {
        //get the input json
        $data = json_decode($request->getContent());

        //Validate if the request has the ifsc code
        if (!isset($data->request->ifsc))
        {
            return "IFSC code must be entered";
        }
        elseif (empty($data->request->ifsc))
        {
            return "IFSC code cannot be blank";
        }

        $ifscObject = new IfscModel();
        $ifsc = $ifscObject->verifyIfscCode($data->request->ifsc);

        if ($ifsc)
        {
            $status = "IFSC Code is correct";
        }
        else
        {
            $status = "IFSC Code is not correct";
        }

        $response['response'] = $status;
        return json_encode($response);

    }

    public function getAllBankCodes(Request $request)
    {
        //get the input json
        $data = json_decode($request->getContent());

        //Validate if the request has the bank name
        if (!isset($data->request->bankName))
        {
            return "Bank name must be entered";
        }
        elseif (empty($data->request->bankName))
        {
            return "Bank name cannot be blank";
        }

        $ifscObject = new IfscModel();
        $ifscCodes = $ifscObject->getAllIfscByBank($data->request->bankName);

        $allCodes = array();
        if (!empty($ifscCodes))
        {
            foreach ($ifscCodes as $code)
            {
                $temp = array();
                $temp['ifsc'] = $code->ifsc;
                $temp['bank_name'] = $code->bank_name;
                $temp['branch_name'] = $code->branch_name;

                $allCodes[] = $temp;
            }
        }

        if (count($allCodes) > 0)
        {
            $response['status'] = 'success';
        }
        else
        {
            $response['status'] = 'failure';
        }

        $response['allCodes'] = $allCodes;

        return json_encode($response);
    }
}