<?php

/**
 * Created by PhpStorm.
 * User: puneet
 * Date: 2/2/17
 * Time: 1:58 AM
 */

namespace App\Http\Models;

use Illuminate\Support\Facades\DB;

class IfscModel
{
    function verifyIfscCode($ifscCode)
    {
        $ifsc = DB::table('ifsc_codes')
            ->where('ifsc','=',$ifscCode)
            ->first();

        return $ifsc;
    }

    function getAllIfscByBank($bankName)
    {
        $ifsc_codes = DB::table('ifsc_codes')
            ->where('bank_name','=',$bankName)
            ->get();

        return $ifsc_codes;
    }
}